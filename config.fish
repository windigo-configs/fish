# Set up one-time calculated variables for the promps
set prompt_host (hostname)

if test -e ~/.config/bash/user_shortcut
	set prompt_user (cat ~/.config/bash/user_shortcut)
else
	set prompt_user $USER
end


# Prompt function
function fish_prompt
	
	printf '%s%s%s@%s%s%s%s→ ' (set_color yellow) $prompt_user (set_color blue) (set_color yellow) $prompt_host (set_color blue)
	set_color normal

end


# Righthand portion of prompt
function fish_right_prompt

	# Print the current working directory
	set_color yellow
	echo -n (pwd)

	# Check whether or not we are in a git repo
	set -l git (git branch --no-color ^/dev/null | sed -e'/^[^*]/d' -e's/*\(.*\)/\1/' | tr -d ' ')

	if test -n $git
		set_color red
		printf '±%s' $git
	end

end
